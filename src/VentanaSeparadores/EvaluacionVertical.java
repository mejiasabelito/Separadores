/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package VentanaSeparadores;

import java.awt.Color;

/**
 *
 * @author Abel
 */
public class EvaluacionVertical extends javax.swing.JFrame {

    /**
     * Creates new form EvaluacionVertical
     */
    public EvaluacionVertical() {
        initComponents();
        this.setTitle("Evaluación Separador Vertical Bifásico");
        this.setLocationRelativeTo(null);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        text8 = new javax.swing.JTextField();
        text7 = new javax.swing.JTextField();
        text6 = new javax.swing.JTextField();
        text5 = new javax.swing.JTextField();
        text10 = new javax.swing.JTextField();
        text9 = new javax.swing.JTextField();
        text2 = new javax.swing.JTextField();
        text1 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        text3 = new javax.swing.JTextField();
        text4 = new javax.swing.JTextField();
        crudoEspumoso = new javax.swing.JCheckBox();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        TrPrim = new javax.swing.JLabel();
        TrSeg = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        TrTer = new javax.swing.JLabel();
        altLiq = new javax.swing.JTextField();
        TpTr1 = new javax.swing.JTextField();
        TpTr2 = new javax.swing.JTextField();
        TpTr3 = new javax.swing.JTextField();
        TGas = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("Datos");

        jLabel2.setText("Presión (lpca)");

        jLabel5.setText("Temperatura (°F)");

        jLabel7.setText("Longitud del Separador (pie)");

        jLabel6.setText("Diametro del Separador (pulg)");

        jLabel9.setText("Gravedad especifica gas");

        jLabel8.setText("Gravedad API");

        jLabel4.setText("Diametro de gota (micron)");

        jLabel14.setText("Factor de comprensibilidad del Gas");

        text7.setText("100");
        text7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                text7ActionPerformed(evt);
            }
        });

        text6.setText("40");

        text5.setText("0.6");

        text10.setText("32");

        text9.setText("11");

        text2.setText("60");

        text1.setText("1000");
        text1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                text1ActionPerformed(evt);
            }
        });

        jButton1.setText("Aceptar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton6.setText("Cancelar");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jButton5.setText("Limpiar");

        jButton2.setText("Guardar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("Imprimir");

        jButton4.setText("Buscar");

        jLabel3.setText("Tasa de petróleo Campo (bpd)");

        jLabel10.setText("Tasa de gas Campo (MMpcsd)");

        text3.setText("2000");

        text4.setText("10");
        text4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                text4ActionPerformed(evt);
            }
        });

        crudoEspumoso.setText("Crudo Espumoso");
        crudoEspumoso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                crudoEspumosoActionPerformed(evt);
            }
        });

        jLabel11.setText("R E S U L T A D O");

        jLabel12.setText("Altura de liquido (pulg)");

        TrPrim.setText("Tasa de petróleo (bpd) tr=1min");

        TrSeg.setText("Tasa de petróleo (bpd) tr=2min");

        jLabel16.setText("Tasa de gas (MMpcsd)");

        TrTer.setText("Tasa de Petróleo (bpd) tr=3min");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel9)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel7)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel2))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(text1, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(text2, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(text9, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(text10, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(text5, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jLabel1)))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel8))
                                .addGap(29, 29, 29)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(text7, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(text6, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel14)
                                .addGap(29, 29, 29)
                                .addComponent(text8, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel10))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(text3, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(text4, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel12)
                            .addComponent(TrPrim)
                            .addComponent(jLabel11)
                            .addComponent(TrTer)
                            .addComponent(jLabel16)
                            .addComponent(TrSeg))
                        .addGap(32, 32, 32)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(TpTr3)
                            .addComponent(TpTr1)
                            .addComponent(TpTr2)
                            .addComponent(TGas, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(altLiq, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButton1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton5))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButton2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton4)))))
                .addGap(33, 33, 33))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(crudoEspumoso)
                .addGap(237, 237, 237))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(text6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(text7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4))
                        .addGap(4, 4, 4)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(text8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel14))
                        .addGap(32, 32, 32)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(text3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(crudoEspumoso)
                        .addGap(1, 1, 1)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(jLabel11))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(text1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(text2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel12)
                            .addComponent(altLiq, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(text9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(TrPrim)
                            .addComponent(TpTr1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(text10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(TrSeg)
                            .addComponent(TpTr2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9)
                            .addComponent(text5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(TrTer)
                            .addComponent(TpTr3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel16)
                            .addComponent(TGas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(59, 59, 59)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton1)
                            .addComponent(jButton6)
                            .addComponent(jButton5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton4)
                            .addComponent(jButton3)
                            .addComponent(jButton2))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(text4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(38, 38, 38))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void text7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_text7ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_text7ActionPerformed

    private void text1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_text1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_text1ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        double dbPresiOpe=Double.parseDouble(text1.getText());
        double dbTempOpe=Double.parseDouble(text2.getText());
        double dbQol=Double.parseDouble(text3.getText());
        double dbQg=Double.parseDouble(text4.getText());
        double dbGEg=Double.parseDouble(text5.getText());
        double dbAPI=Double.parseDouble(text6.getText());
        double dbDmGota=Double.parseDouble(text7.getText());
        //double z=Double.parseDouble(text8.getText());
        double Lss =Double.parseDouble(text9.getText());
        double d =Double.parseDouble(text10.getText());
        
        //Factor de comprensibilidad Z
       dbTempOpe = dbTempOpe + 460;
         
        double dbPpr = 756.8-(131*(dbGEg))-(3.6*Math.pow(dbGEg,2));
        dbPpr = dbPresiOpe/dbPpr;
        double dbTpr = (169.2 + (349.5*dbGEg)-(74*Math.pow(dbGEg,2)));
        dbTpr=dbTempOpe/dbTpr;
        double z=1;
        double zold=0;
        while(Math.abs((z-zold)/(z+(1*Math.pow(10,-9))))>=0.001){
            zold=z;
            double M=(0.27*dbPpr)/(z*dbTpr);
            z=1+(0.3265-(1.07/dbTpr))-(0.5339/dbTpr)+(0.01569/(Math.pow(dbTpr, 2)))-((0.05165/(Math.pow(dbTpr,5)))*M);
            z=z+(0.5475-(0.7361/dbTpr))+(0.1844/Math.pow(dbTpr,2))*(Math.pow(M,2));
            z=z-(0.1056*(-0.7361/dbTpr)+(0.1844/Math.pow(dbTpr,2)))*Math.pow(M,5);
            z=z+(0.6134*(1+(0.721*Math.pow(M,2)))*(Math.pow(M,2)/(Math.pow(dbTpr,3))))*Math.exp(-0.721*Math.pow(M, 2));
        }
        //Viscosidad gas

        double dbPMg = (dbGEg*28.97);
        double dbDensig= ((dbPMg * dbPresiOpe)/(10.73*(dbTempOpe)*z));
        double pg = (dbDensig/62.492);

        //parametros A,B,C
        double A =(((9.379+0.0167*dbPMg)*Math.pow(dbTempOpe-460,1.5))/(209.2+(19.26*dbPMg)+(dbTempOpe-460)));
        double B = 3.448 + (986.4/(dbTempOpe-460))+(0.01009*dbPMg);
        double C = 2.447 - (0.2224*B);

        double dbVicGas = A*Math.exp(B*Math.pow(dbDensig,C))*Math.pow(10,-4);

        //coeficiente de arrastre
        double dbGEol = (141.5/(131.5+dbAPI));
        double dbDensiOl = dbGEol *62.4;
        double dbVt= 0.0204*Math.sqrt(((dbDensiOl-dbDensig)*dbDmGota)/dbDensig);
        double Re= 0.0049*((dbDensig*dbDmGota*dbVt)/dbVicGas);
        double Cd =24/Re + (3/Math.sqrt(Re)) +0.34;
        double dbVt2 = 0.0119*Math.sqrt(((dbDensiOl-dbDensig)/dbDensig)*(dbDmGota/Cd));
        while (dbVt-dbVt2 >= 0.00001){
            dbVt = dbVt2;
            Re= 0.0049*((dbDensig*dbDmGota*dbVt)/dbVicGas);
            Cd = (24/Re) + (3/Math.sqrt(Re)) +0.34;
            dbVt2 = 0.0119*Math.sqrt(((dbDensiOl-dbDensig)/dbDensig)*(dbDmGota/Cd));
        }

        //constante de soudors y brown

        double k = Math.sqrt((dbDensig/(dbDensiOl-dbDensig))*(Cd/dbDmGota));
        // paso 5
        double h = 12*Lss-d-40;
        //paso 6
        int []intTr= new int[3];
        double []Q1 = new double[3];
        if(!crudoEspumoso.isSelected()){
            intTr[0]=1;
            intTr[1]=2;
            intTr[2]=3;
        }else{
            intTr[0]=4;
            intTr[1]=8;
            intTr[2]=12;
        }
        for (int y=0;y<3;y++){
            Q1[y]=((d*d)*h*0.12)/(intTr[y]);
        }
        //paso 7
        double Qg=((d*d*dbPresiOpe))/(5040*(dbTempOpe)*z*k);
        
        altLiq.setText(String.valueOf(round(h,2)));
        altLiq.setBackground(Color.decode("#cceec7"));
        altLiq.setEditable(false);
        TrPrim.setText("Tasa de petróleo (bpd) tr="+intTr[0]+"min");
        TrSeg.setText("Tasa de petróleo (bpd) tr="+intTr[1]+"min");
        TrTer.setText("Tasa de petróleo (bpd) tr="+intTr[2]+"min");
        TpTr1.setText(String.valueOf(round(Q1[0],2)));
        if(dbQol<Q1[0]){
            TpTr1.setBackground(Color.decode("#cceec7"));
            TpTr1.setEditable(false);
        }else{
            TpTr1.setBackground(Color.red);
            TpTr1.setEditable(false);
        }
        TpTr2.setText(String.valueOf(round(Q1[1],2)));
        if(dbQol<Q1[1]){
            TpTr2.setBackground(Color.decode("#cceec7"));
            TpTr2.setEditable(false);
        }else{
            TpTr2.setBackground(Color.red);
            TpTr2.setEditable(false);
        }
        TpTr3.setText(String.valueOf(round(Q1[2],2)));
        if(dbQol<Q1[2]){
            TpTr3.setBackground(Color.decode("#cceec7"));
            TpTr3.setEditable(false);
        }else{
            TpTr3.setBackground(Color.red);
            TpTr3.setEditable(false);
        }
        TGas.setText(String.valueOf(round(Qg,2)));
        TGas.setBackground(Color.decode("#cceec7"));
        TGas.setEditable(false);
        

    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton2ActionPerformed

    private void crudoEspumosoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_crudoEspumosoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_crudoEspumosoActionPerformed

    private void text4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_text4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_text4ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
       MenuPrincipal mp= new MenuPrincipal();
        mp.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_jButton6ActionPerformed

    /**
     * @param args the command line arguments
     */

     public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

    long factor = (long) Math.pow(10, places);
    value = value * factor;
    long tmp = Math.round(value);
    return (double) tmp / factor; //To change body of generated methods, choose Tools | Templates.
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField TGas;
    private javax.swing.JTextField TpTr1;
    private javax.swing.JTextField TpTr2;
    private javax.swing.JTextField TpTr3;
    private javax.swing.JLabel TrPrim;
    private javax.swing.JLabel TrSeg;
    private javax.swing.JLabel TrTer;
    private javax.swing.JTextField altLiq;
    private javax.swing.JCheckBox crudoEspumoso;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JTextField text1;
    private javax.swing.JTextField text10;
    private javax.swing.JTextField text2;
    private javax.swing.JTextField text3;
    private javax.swing.JTextField text4;
    private javax.swing.JTextField text5;
    private javax.swing.JTextField text6;
    private javax.swing.JTextField text7;
    private javax.swing.JTextField text8;
    private javax.swing.JTextField text9;
    // End of variables declaration//GEN-END:variables
}
